/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefentoets2;

import java.util.Scanner;

/**
 *
 * @author Jeffrey
 * Programma voor getallen raden.. etc etc etc.
 */
public class OefenToets2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Declareren constanten, variabelen en arrays:
        final int LIMIET = 3;
        final int MINIMUM = 1;
        final int MAXIMUM = 8;
        Scanner input = new Scanner(System.in);
        int aantalPogingen = 0;
        int[] poging = new int[LIMIET];
        int[] randomGetallen = new int[LIMIET]; // default waarde 0 dus gelijk

        // Druk eigen gegevens af:
        System.out.println("StudentX, XX100, 5008123456");
        System.out.println(" ----------------------------------- ");
        System.out.println(" \t Raad de " + LIMIET + " getallen \t  ");
        System.out.println(" ----------------------------------- ");
        
        // Genereer verschillende random getallen:
        while ( ! alleOngelijk(randomGetallen) ) {
            for (int i = 0; i < randomGetallen.length; i++) {
                randomGetallen[i] = MINIMUM + 
                        (int) (Math.random() * MAXIMUM);
            }
        }        
        // Blijf om getallen vragen zolang ze niet allemaal goed geraden zijn:
        int aantalGoed = 0;
        while ( aantalGoed != LIMIET) {
            aantalGoed = 0;
            // Lees aantal verschillende getallen:
            do {
                // Geef aan welke getallen ingevoerd moeten worden:
                System.out.print("Geef " + LIMIET 
                        + " verschillende getallen tussen "
                        + MINIMUM + " en " + MAXIMUM
                        + ", gescheiden door spaties: ");
                for (int i = 0; i < LIMIET; i++)
                    poging[i] = input.nextInt();
            } while ( ! alleOngelijk(poging) );
            // kijk of ze goed geraden zijn:
            for (int pogingGetal: poging)
                if (komtVoorIn(pogingGetal, randomGetallen))
                    aantalGoed++;
            aantalPogingen++;
            // Toon hoeveel getallen goed geraden zijn:
            System.out.println("Aantal correcte getallen = " + aantalGoed);           
        }
        // Geef aantal aantalPogingen terug en de random getallen:
        System.out.println("\nU heeft " + aantalPogingen + " keer geraden.");
        System.out.print("De te raden getallen waren: ");
        for (int randomGetal: randomGetallen)
            System.out.print(" " + randomGetal);
        System.out.println();
    }
    /**
     * Methode om te bepalen of een zoekgetal in een lijst voorkomt.
     * 
     * @param zoekgetal is het te zoeken getal
     * @param lijst waarin gezocht wordt
     * @return true als zoekGetal in lijst voorkomt
     * 
     **/
     public static boolean komtVoorIn(int zoekgetal, int[] lijst){  
        for (int e: lijst) {
            if (zoekgetal == e)
                return true;
        }
        return false;
    }
    /**
     * Methode om te bepalen of alle elementen in een lijst ongelijk zijn.
     * 
     * @param lijst met te vergelijken elementen
     * @return true als alle elementen verschillend zijn
     * 
     **/
    public static boolean alleOngelijk(int[] lijst){
        for (int i=0; i < lijst.length; i++)
            for (int j=i+1; j < lijst.length; j++)
                if (lijst[i] == lijst[j])
                    return false;
        return true;
    }
    
}